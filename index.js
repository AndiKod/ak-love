/**
 *  Love Package
 * 
 *  Just ads a ❤️ to any String you throw at him.
 */

module.exports = (str) => {
    return `${str}❤️`;
} 