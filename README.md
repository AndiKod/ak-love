## All you need is Love

That tiny package adds ❤️ after a given word.

### Install
```
npm i ak-love
```

### Import
```js
const love = require('ak-love');
```

### Show some Love
```js
love('Pizza'); // turns into: Pizza❤️
```

Right, that's not much, 
but the package was made 
just for fun and Coding❤️